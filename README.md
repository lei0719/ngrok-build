# ngrok-build
快速编译ngrok，实现http内外网穿透，tcp隧道

## 编译ngrok
1.准备一台centos7的环境

2.执行命令
```shell
chmod 777 build-ngrok.sh
./build-ngrok.sh {你的域名}
```

3.添加域名解析 

添加域名解析`tunnel.{你的域名}`到当前主机

4.开启防火墙

开启防火墙端口，80，443，4443

## 启动ngrok服务端
```shell
ngrokd -domain {你提供的泛域名} -httpsAddr :443 -httpAddr :80 -tunnelAddr :4443
```

## 使用客户端
拷贝bin目录下`ngrok.yml`和bin目录下对应平台的nginx执行文件到你的客户端平台上，执行命令
``` cmd
 .\ngrok -config ngrok.yml -subdomain test {要映射到本地的资源地址必须带上端口}
```

## 这个是极简的编译，如果想了解编译细节
[请点击这里](http://blog.csdn.net/hulei19900322/article/details/78198742)